import 'es6-promise/auto'
import 'babel-polyfill'
import 'whatwg-fetch'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from '../core/store-mobile'
import './registerServiceWorker'

Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App),
  created() {
  },
}).$mount('#app')
