import Vue from 'vue'
import Router from 'vue-router'
import Home from './View.vue'
import Check from './Check'

Vue.use(Router)

export default new Router({
  routes: [
    /*{
      path: '/check',
      name: 'check',
      component: Check
    },//*/
    {
      path: '/:idRoot?/:id?',
      name: 'home',
      component: Home
    }
  ],
})
