var FtpDeploy = require('ftp-deploy');
var ftpDeploy = new FtpDeploy();

var config = {
    user: "j693917_opros-web-mobil",                   // NOTE that this was username in 1.x
    password: "opros-web-mobil",                          // optional, prompted if none given
    host: "j693917.myjino.ru",
    port: 21,
    localRoot: __dirname + '/dist',
    remoteRoot: '/',
     include: ['*', '**/*'],          // this would upload everything except dot files
    deleteRemote: false,           // delete ALL existing files at destination before uploading, if true
    forcePasv: false                     // Passive mode is forced (EPSV command is not sent)
}

ftpDeploy.deploy(config)
    .then(res => console.log('finished deploy'))
    .catch(err => console.log(err))
